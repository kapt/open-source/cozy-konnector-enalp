Changelog
=========


v0.0.5 (2022-07-07)
-------------------

Bug fixes
~~~~~~~~~
- Handle negative floats in normalizePrice func [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]


v0.0.4 (2022-07-07)
-------------------

Bug fixes
~~~~~~~~~
- Mistake in invoice price normalization [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]


v0.0.3 (2022-07-06)
-------------------

Bug fixes
~~~~~~~~~
- Invoices files names generation when amounts contains non-digit chars
  in the extranet [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- Remove old reference to .travis.yml file [Benjamin PIERRE]


v0.0.2 (2022-02-10)
-------------------

Documentation
~~~~~~~~~~~~~
- Update changelog [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- MAINT: Fix lint problems dues to babel-eslint old version : -
  https://github.com/eslint/eslint/issues/12117 -
  https://yarnpkg.com/cli/set/resolution - yarn set resolution babel-
  eslint@npm:10.0.1 ^10.1.0 [Benjamin PIERRE]
- Update dependencies for CI [Benjamin PIERRE]
- Begin to configure CI [Benjamin PIERRE]


v0.0.1 (2022-02-10)
-------------------

Features
~~~~~~~~
- Add onlyLastNMonths parameter to retrieve invoices only from last N
  months [Benjamin PIERRE]
- Changed input parameters to be in camelcase [Benjamin PIERRE]
- Add locale for contract number field [Benjamin PIERRE]
- Updated manifest.konnector with folderPath field [Benjamin PIERRE]
- Re-set contact id field in manifest.konnector [Benjamin PIERRE]
- Add new 'contractId' field : - Allows to run the connector on a single
  contract ID to prevent connector execution from taking too much time
  [Benjamin PIERRE]

Bug fixes
~~~~~~~~~
- Invalid folderPath in konnector manifest [Benjamin PIERRE]
- Remove contractId field from konnector [Benjamin PIERRE]
- Malformed konnector manifest [Benjamin PIERRE]
- Invalid manifest konnector [Benjamin PIERRE]

Documentation
~~~~~~~~~~~~~
- Update konnector for new contract number form field [Benjamin PIERRE]

Maintenance
~~~~~~~~~~~
- Configure bumpversion [Benjamin PIERRE]
- Configure bumpversion [Benjamin PIERRE]
- Switch to node v16 and yarn v3 [Benjamin PIERRE]
- Format code + update gitignore [Benjamin PIERRE]
- Fix build command dependencies [Benjamin PIERRE]
- Fix a mistake in package.json [Benjamin PIERRE]

Other
~~~~~
- First version of konnector:     - Downloads all the bills of all the
  contracts of the account     - Requires improvements on priorizing the
  contracts to be downloaded first because of the jobs running timeout
  of 5 minutes that doesnt allows all of the invoices to be retrieved in
  a single time [Benjamin PIERRE]
- Revert "Configure SAST in `.gitlab-ci.yml`, creating this file if it
  does not already exist" [Benjamin PIERRE]

  This reverts commit cec166d0ff5f367cb904ab3213d4e8d92a8bfb79

- Configure SAST in `.gitlab-ci.yml`, creating this file if it does not
  already exist [PIERRE Benjamin]




.. Generated by gitchangelog
